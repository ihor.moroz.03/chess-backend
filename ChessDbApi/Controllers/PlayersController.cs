﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ChessDbApi.Data;
using System.Text.RegularExpressions;

namespace ChessDbApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase
    {
        private readonly DataContext _context;

        public PlayersController(DataContext context)
        {
            _context = context;
        }

        // GET: api/Players
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PlayerEntity>>> GetPlayers()
        {
            if (_context.Players == null)
            {
                return NotFound();
            }
            return await _context.Players.ToListAsync();
        }

        // GET: api/Players/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PlayerEntity>> GetPlayerEntity(int id)
        {
            if (_context.Players == null)
            {
                return NotFound();
            }
            var playerEntity = await _context.Players.FindAsync(id);

            if (playerEntity == null)
            {
                return NotFound();
            }

            return playerEntity;
        }

        // PUT: api/Players/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlayerEntity(int id, PlayerEntity playerEntity)
        {
            if (id != playerEntity.Id)
            {
                return BadRequest();
            }

            _context.Entry(playerEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlayerEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Players
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PlayerEntity>> PostPlayerEntity(PlayerEntity playerEntity)
        {
            if (_context.Players == null)
            {
                return Problem("Entity set 'DataContext.Players'  is null.");
            }

            Regex usernameValidator = new(@"[\w\d]+");
            Regex passwordValidator = new(@"(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[\w\d]{8,}");
            //Regex nameValidator = new(@"\w+");

            List<string> problems = new List<string>();
            if (!usernameValidator.IsMatch(playerEntity.Username ?? ""))
                problems.Add("Invalid username");
            if (!passwordValidator.IsMatch(playerEntity.Password ?? ""))
                problems.Add("Invalid password");
            //if (!nameValidator.IsMatch(playerEntity.Name ?? ""))
            //problems.Add("Invalid name");
            if (problems.Count > 0)
                return Problem("Following problems occured:\n" + string.Join("\n", problems));

            _context.Players.Add(playerEntity);
            await _context.SaveChangesAsync();

            return Ok();
            //return CreatedAtAction("GetPlayerEntity", new { id = playerEntity.Id }, playerEntity);
        }

        // DELETE: api/Players/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlayerEntity(int id)
        {
            if (_context.Players == null)
            {
                return NotFound();
            }
            var playerEntity = await _context.Players.FindAsync(id);
            if (playerEntity == null)
            {
                return NotFound();
            }

            _context.Players.Remove(playerEntity);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PlayerEntityExists(int id)
        {
            return (_context.Players?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
