﻿using Microsoft.EntityFrameworkCore;

namespace ChessDbApi.Data
{
    public class DataContext : DbContext
    {
        public DbSet<PlayerEntity> Players { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data Source = Chess.db");
        }
    }
}
