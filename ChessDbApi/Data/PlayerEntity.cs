﻿namespace ChessDbApi.Data
{
    public class PlayerEntity
    {
        public int Id { get; set; }

        public string? Username { get; set; }

        public string? Password { get; set; }

        public string? Name { get; set; }

        public int? Age { get; set; }
    }
}
